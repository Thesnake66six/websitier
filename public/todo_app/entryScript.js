let enterButton = document.getElementById("enter");
let input = document.getElementById("input");
let list = document.getElementById("list");

function toggleDone(event) {
    event.target.parentNode.parentNode.parentNode.firstChild.classList.toggle("done");
}

function deleteItem(e) {
    e.target.parentNode.parentNode.parentNode.remove();
}

function addItemEvent() {
    if (input.value.length != 0) {
        addItem(input.value);
    }
    input.value = "";
}

function addItem(text) {
    let itemDiv = document.createElement("div");
    itemDiv.className = "item";
        let itemP = document.createElement("p");
        itemP.textContent = text;

    itemDiv.appendChild(itemP);
        let itemButtonsDiv = document.createElement("div");
        itemButtonsDiv.className = "button-container";
            let itemCheckButton = document.createElement("button");
            itemCheckButton.className = "button check";
                let itemCheckButtonSpan = document.createElement("span");
                itemCheckButtonSpan.className = "material-symbols-outlined";
                itemCheckButtonSpan.textContent = "check";
                itemCheckButton.appendChild(itemCheckButtonSpan);
            itemCheckButton.addEventListener('click', toggleDone);

        itemButtonsDiv.appendChild(itemCheckButton);
            let itemDeleteButton = document.createElement("button");
            itemDeleteButton.className = "button delete";
                let itemDeleteButtonSpan = document.createElement("span");
                itemDeleteButtonSpan.className = "material-symbols-outlined";
                itemDeleteButtonSpan.textContent = "close";
                itemDeleteButton.appendChild(itemDeleteButtonSpan);
            itemDeleteButton.addEventListener('click', deleteItem);
        itemButtonsDiv.appendChild(itemDeleteButton);

    itemDiv.appendChild(itemButtonsDiv);
    list.appendChild(itemDiv);

}

enterButton.addEventListener("click", addItemEvent);

input.addEventListener("keydown", function(event) {
    if (event.key === "Enter") {
        addItemEvent() ;
    }
})

Sanity